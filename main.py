import numpy as np
from sklearn.ensemble import (RandomForestClassifier, AdaBoostClassifier)
from sklearn.tree import DecisionTreeClassifier
from sklearn import clone
from randomForest import MyRandomForestClassifier
from sklearn.metrics import confusion_matrix

def crossValidation(X, y, model, k=5):
   n = len(X)
   batchSize = n / k
   accuracies = []

   name = str(type(model)).split(".")[-1][:-2][:-len("Classifier")]
   print ("Classifier %s" % name)
   cms = []
   for i in xrange(n / batchSize):
      if (i == (n / batchSize)):
         valInd = range(batchSize * i, n)
      else:
         valInd = range(batchSize * i, batchSize * (i + 1))
      trainInd = np.delete(np.arange(n), valInd)

      curModel = clone(model)

      curModel.fit(X[trainInd], y[trainInd])

      accuracy = curModel.score(X[valInd], y[valInd])
      accuracies.append(accuracy)
      print("Batch %d, confusion matrix:" %(i))
      cm = confusion_matrix(y[valInd], curModel.predict(X[valInd]))
      print(cm)
      cms.append(cm)

   print("\nAverage conf matrix:")
   print(np.mean(cms, axis=0))

   print('\nAccuracy for %s is %f\n' % (name, np.mean(np.asarray(accuracies))))


dtype = ['S2', 'float', 'S3', 'S8', 'S3', 'S3', 'float', 'float', 'float', 'float',
         'float', 'float', 'float', 'float', 'float', 'float', 'float', 'float', 'float', 'float', 'S5']
data = np.genfromtxt('churn.csv', delimiter=',', names=True, dtype=dtype)

X = np.zeros((len(data), len(data[0])-2), float)

categorical_vars = ['State', 'Area_Code']
id_var = 'Phone'
binary_vars = ["Intl_Plan", "VMail_Plan"]

res_var = 'Churn'
float_vars = ["Account_Length", "VMail_Message", "Day_Mins", "Day_Calls", "Day_Charge",
              "Eve_Mins", "Eve_Calls", "Eve_Charge", "Night_Mins", "Night_Calls", "Night_Charge",
              "Intl_Mins", "Intl_Calls", "Intl_Charge", "CustServ_Calls"]

y = np.array([float(x!='False') for x in data[res_var]])

i=0
for s in float_vars:
   X[:,i] = data[s]
   i+=1

for s in binary_vars:
   X[:,i] = np.array([float(x=='yes') for x in data[s]])
   i+=1

for s in categorical_vars:
   freq = []
   categories = list(set(data[s]))
   for c in categories:
      freq.append(sum(y[data[s]==c]))
   sorted_ind = np.argsort(freq)
   max = len(sorted_ind) - 1
   newcolumn = [float(max - sorted_ind[categories.index(x)]) for x in data[s]]
   X[:, i] = newcolumn
   i+=1


models = [MyRandomForestClassifier(n_trees=5),
          RandomForestClassifier(n_estimators=5),
          AdaBoostClassifier(DecisionTreeClassifier(max_depth=3),
                             n_estimators=5)]

for model in models:
   crossValidation(X, y, model)
