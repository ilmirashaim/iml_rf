import numpy as np
from sklearn.base import (BaseEstimator, ClassifierMixin)


class Node(object):
   def __init__(self):
      self.left = None
      self.right = None
      self.is_leaf = False
      self.n_members = 0

class MyRandomForestClassifier(BaseEstimator, ClassifierMixin):
   def __init__(self, n_trees=10, min_node_size=7, n_var='auto'):
      self.n_trees = n_trees
      self.n_var = n_var
      self.min_node_size = min_node_size
      self.estimator_params=("n_var", "min_node_size")

   def fit(self, X, y):

      self.trees_ = []
      self.n_samples_ = X.shape[0]
      self.classes_ = list(set(y))
      self.n_classes_ = len(self.classes_)
      self.n_features_ = X.shape[1]

      X_sep = []
      n_for_class = self.n_samples_ / self.n_classes_
      for cl in self.classes_:
         X_sep.append(X[y==cl,:])

      for b in xrange(self.n_trees):
         # draw a bootstrap sample
         j=0
         X_train = np.zeros((n_for_class*self.n_classes_, self.n_features_))
         y_train = np.zeros(n_for_class*self.n_classes_)
         for cl in self.classes_:
            inds = np.random.randint(0, len(X_sep[j]), n_for_class)
            i = 0
            for ind in inds:
               X_train[i] = X_sep[j][ind]
               y_train[i] = cl
               i+=1
            j+=1

         tree = MyRandomTreeClassifier()
         self.trees_.append(tree)
         tree.set_params(**dict((p, getattr(self, p)) for p in self.estimator_params))

         # grow a random-forest tree to the bootstrapped data
         tree.fit(X_train, y_train)

      return self

   def predict(self, X):
      n_samples = X.shape[0]
      y = np.zeros(n_samples)
      predictions = np.zeros((n_samples, self.n_trees))
      j = 0
      for tree in self.trees_:
         predictions[:,j] = tree.predict(X)
         j+=1
      for i in xrange(n_samples):
         bincounts = np.bincount(np.asarray(predictions[i, :], int))
         y[i] = np.argmax(bincounts)
      return y


class MyRandomTreeClassifier(BaseEstimator, ClassifierMixin):
   def __init__(self, min_node_size=1, n_var='auto'):
      self.min_node_size = min_node_size
      self.n_var = n_var
      self.tree_ = []

   def fit(self, X, y):
      self.n_features_ = X.shape[1]
      if isinstance(self.n_var, basestring):
         if self.n_var == "auto":
            n_var = max(1, int(np.sqrt(self.n_features_)))
         elif self.n_var == "log2":
            n_var = max(1, int(np.log2(self.n_features_)))
         else:
            raise ValueError('Invalid value for number of variables')
      elif self.n_var is None:
         n_var = self.n_features_
      elif isinstance(self.n_var, int):
         n_var = self.n_var
      else:  # float
         if self.n_var > 0.0:
             n_var = max(1, int(self.n_var * self.n_features_))
         else:
             n_var = 0

      self.n_var = n_var
      self.split(X, y)
      return

   def predict(self, X):
      n_samples = X.shape[0]
      y = np.zeros(n_samples)
      for i in xrange(n_samples):
         y[i] = self.predict_one(X[i,:])
      return y

   def predict_one(self, x):
      i = 0
      node = self.tree_[i]
      while node[0] is not None:
         if(x[node[0]] > node[1]):
            i = (i+1)*2
         else:
            i = (i+1)*2 - 1
         if(i < len(self.tree_)):
            node = self.tree_[i]
         else:
            break
      return node[2]

   def split(self, X, y):
      if(len(y) == 0):
         return
      classes = set(y)
      n_classes_ = len(classes)
      n_samples = X.shape[0]

      if n_classes_ == 1:
         self.tree_.append((None, None, y[0]))
         return

      max_class = np.argmax(np.bincount(np.asarray(y, int)))
      if n_samples <= self.min_node_size:
         self.tree_.append((None, None, max_class))
         return

      # pick the best variable/split-point among the n_vars variables
      attr_n, val = self.select_attr(X, y)

      self.tree_.append((attr_n, val, max_class))

      # split the node into two daughter nodes
      X_right, y_right, X_left, y_left  = self.get_split(X, y, attr_n, val)

      # recursively repeating the following steps for each terminal node of
      # the tree, until the minimum node size (min_node_size) is reached.
      if(len(y_left) > self.min_node_size):
         self.split(X_left, y_left)
      if(len(y_right) > self.min_node_size):
         self.split(X_right, y_right)
      return

   def select_attr(self, X, y):
      # select n_var variables at random from the n_features_ variables
      attrs = np.random.permutation(self.n_features_)[:self.n_var]
      importances = []
      split_vals = []
      for attr_n in attrs:
         importance, val = self.compute_attr_importance(X, y, attr_n)
         importances.append(importance)
         split_vals.append(val)
      ind = np.argmax(importances)
      return attrs[ind], split_vals[ind]

   def compute_attr_importance(self, X, y, attr_n):
      values = list(set(X[:, attr_n]))

      n_values = len(values)
      if n_values > 50:
         max_val = max(values)
         values = np.linspace(0, max_val, 20).tolist()
      if(n_values > 2):
         values.remove(max(values))
      if(n_values > 1):
         values.remove(min(values))
      importances = []

      for val in values:
         importance = self.compute_split_importance(X, y, attr_n, val)
         importances.append(importance)
      ind = np.argmax(importances)
      return (importances[ind], values[ind])

   def compute_split_importance(self, X, y, attr_n ,split_val):
      X_right, y_right, X_left, y_left  = self.get_split(X, y, attr_n, split_val)
      if(len(y_right) < self.min_node_size or len(y_left) < self.min_node_size):
         return 0

      n_samples = X.shape[0]

      left_impurity = self.compute_node_impurity(X_left, y_left)
      right_impurity = self.compute_node_impurity(X_right, y_right)

      gini_split = left_impurity*(float(len(y_left))/float(n_samples)) +\
                   right_impurity*(float(len(y_right))/float(n_samples))

      return 1 - gini_split

   def compute_node_impurity(self, X, y):
      classes = set(y)
      n_samples = X.shape[0]

      impurity = 1
      for cl in classes:
         pk = (sum(y == cl))/float(n_samples)
         impurity -= pk*pk

      return impurity

   def get_split(self, X, y, attr_n, val):
      ind_right = X[:, attr_n] > val
      X_right = X[ind_right, :]
      y_right = y[ind_right]


      ind_left = ~ind_right
      X_left = X[ind_left, :]
      y_left = y[ind_left]
      return X_right, y_right, X_left, y_left

